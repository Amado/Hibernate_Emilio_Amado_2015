package herencia.modelo;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="NM")
public class Normal extends Persona {
	private String ocupacion;

	public Normal() {
	}

	public Normal(String nombre, int edad, String ocupacion) {
		super(nombre, edad);
		this.ocupacion = ocupacion;
	}

	public String getOcupacion() {
		return ocupacion;
	}

	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}

	@Override
	public String toString() {
		return "Normal [ocupacion=" + ocupacion + ", Id=" + getId()
				+ ", Nombre=" + getNombre() + "]";
	}
	
	
}