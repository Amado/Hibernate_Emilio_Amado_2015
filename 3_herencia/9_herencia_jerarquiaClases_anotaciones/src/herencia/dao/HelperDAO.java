/*
 * AbstractDAO.java
 *
 * Creada el 24/07/2010, 01:22:33 PM
 *
 * Clase Java desarrollada por Alex para el blog http://javatutoriales.blogspot.com/ el día 24/07/2010
 *
 * Para informacion sobre el uso de esta clase, asi como bugs, actualizaciones, o mejoras enviar un mail a
 * programadorjavablog@gmail.com
 *
 */

package herencia.dao;



import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class HelperDAO{
	
	private static Session sesion;

	public static void iniciaOperacion() {
		sesion = HibernateUtil.getSessionFactory().openSession();
		sesion.getTransaction().begin();
	}

	public static void terminaOperacion() {
		sesion.getTransaction().commit();
		sesion.close();
	}

	public static void manejaExcepcion(HibernateException he)
			throws HibernateException {
		sesion.getTransaction().rollback();
		throw he;
	}

	public static Session getSession() {
		return sesion;
	}

	public static void almacenaEntidad(Object entidad)
			throws HibernateException {

		try {
			iniciaOperacion();
			getSession().saveOrUpdate(entidad);
			getSession().flush();
		} catch (HibernateException he) {
			manejaExcepcion(he);
		} finally {
			terminaOperacion();
		}
	}
	
	public static void eliminaEntidad(Object entidad) throws HibernateException{

	    try{
	        iniciaOperacion();
	        getSession().delete(entidad);
	        getSession().flush();
	    } catch(HibernateException he) {
	        manejaExcepcion(he);
	    } finally {
	        terminaOperacion();
	    }
	}

	/*
	 * Para recuperar las entidades hacemos uso de Generics T, lo que quiere
	 * decir que el objeto entidad recuperado, puede ser de cualquier tipo para
	 * hacer lo mismo podr�amos usar el tipo de ojeto padre de todos (Object) y
	 * en el caso de tener que declara un Class, usar�amos Class<Object>, o
	 * Class<?> pero esto se considera malas pr�cticas, ya que puede dar errores
	 * en tiempo de ejecuci�n para evitarlos deber�amos usar "instanceof", por
	 * ello se usa el objeto T (generics), que nos obliga a usar un tipo de
	 * objeto especifico en la llamada del m�todo, as� es imposible que haya
	 * errores de ejecuci�n y queda un c�digo m�s limpio y sin usar "instanceof"
	 */

	/*
	 * Ejemplos de llamada del m�todo :
	 * 
	 * 		getEntidad(new Long(22), Usuario.class) ; 
	 * 		getEntidad(new Long(3), Compra.class) ;
	 */

	
	
	/*
	 * Recupera la entidad de tipo <T> con id especificada, almacenada en la db
	 */
	public static <T> T getEntidad(Serializable id, Class<T> claseEntidad)
			throws HibernateException {

		T objetoRecuperado = null;

		try {
			iniciaOperacion();
			objetoRecuperado = (T) getSession().get(claseEntidad, id);
		} catch (HibernateException he) {
			manejaExcepcion(he);
		} finally {
			terminaOperacion();
		}

		return objetoRecuperado;
	}

	/*
	 * Recupera todas las entidades de tipo <T> almacenadas en la db
	 */
	public static <T> List<T> getListaEntidades(Class<T> claseEntidad)
			throws HibernateException {

		List<T> listaResultado = null;

		try {
			iniciaOperacion();
			listaResultado = (List<T>) getSession().createQuery("FROM " + claseEntidad.getSimpleName()).list();
		} catch (HibernateException he) {
			manejaExcepcion(he);
		} finally {
			terminaOperacion();
		}

		return listaResultado;
	}
	
}
