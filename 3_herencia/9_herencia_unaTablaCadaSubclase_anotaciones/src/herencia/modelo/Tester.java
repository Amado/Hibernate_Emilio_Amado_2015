package herencia.modelo;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name="testers")
public class Tester extends Tecnologo {
	private String herramientaDeTesteo;

	public Tester() {
	}

	public Tester(String nombre, int edad, int aniosDeEstudios,
			String herramientaDeTesteo) {
		super(nombre, edad, aniosDeEstudios);
		this.herramientaDeTesteo = herramientaDeTesteo;
	}

	public String getHerramientaDeTesteo() {
		return herramientaDeTesteo;
	}

	public void setHerramientaDeTesteo(String herramientaDeTesteo) {
		this.herramientaDeTesteo = herramientaDeTesteo;
	}

	@Override
	public String toString() {
		return "Tester [herramientaDeTesteo=" + herramientaDeTesteo
				+ ", Id=" + getId() + ", Nombre=" + getNombre() + "]";
	}
	
	
}