package herencia.main;

import herencia.dao.HelperDAO;
import herencia.modelo.Normal;
import herencia.modelo.Persona;
import herencia.modelo.Programador;
import herencia.modelo.Tecnologo;
import herencia.modelo.Tester;

public class Main {

	public static <T> void main(String[] args) {

	    Normal      normal       = new Normal("normal", 21, "Empleado");
	    Tecnologo   tecnologo    = new Tecnologo("tecnologo", 24, 4);
	    Programador programador1 = new Programador("primer programador", 25, 4, "java", 4);
	    Programador programador2 = new Programador("segundo programador", 25, 5, "java", 2);
	    Tester      tester       = new Tester("tester", 18, 3, "JUnit");
	    
	    HelperDAO.almacenaEntidad(normal);
	    HelperDAO.almacenaEntidad(tecnologo);
	    HelperDAO.almacenaEntidad(programador1);
	    HelperDAO.almacenaEntidad(programador2);
	    HelperDAO.almacenaEntidad(tester);

        T t = (T) HelperDAO.getEntidad(tecnologo.getId(), Persona.class);
        System.out.println("\n" + t.toString());
	}

}
