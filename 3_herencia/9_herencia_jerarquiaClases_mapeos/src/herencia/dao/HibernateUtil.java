package herencia.dao;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static final SessionFactory sessionFactory;

	//en un bloque de inicialización estático, inicializamos el objeto
	//Sessionfactory en el momento en que la clase se carga sobre el JVM
	static {
		try {
			sessionFactory = new Configuration().
					configure().//hibernate busca el fichero de config "hibernate.cfg.xml"
					buildSessionFactory();//hibernate carga la config del fichero
		} catch (HibernateException he) {
			System.err.println("Ocurrió un error en la inicialización de la SessionFactory: "
							+ he);
			throw new ExceptionInInitializerError(he);
		}
	}

	//retorna la sessionfactory creada
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
