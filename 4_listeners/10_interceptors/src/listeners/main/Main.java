/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listeners.main;

import listeners.dao.HelperDAO;
import listeners.modelo.Usuario;


public class Main {

	public static <T> void main(String[] args) {
		
	    Usuario usuario1 = new Usuario("usuario 1", "username usuario 1", "password usuario 1");
	    Usuario usuario2 = new Usuario("usuario 2", "username usuario 2", "password usuario 2");
	    Usuario usuario3 = new Usuario("usuario 3", "username usuario 3", "password usuario 3");
	    Usuario usuario4 = new Usuario("usuario 4", "username usuario 4", "password usuario 4");
	    Usuario usuario5 = new Usuario("usuario 5", "username usuario 5", "password usuario 5");

	    HelperDAO.almacenaEntidad(usuario1);
	    HelperDAO.almacenaEntidad(usuario2);
	    HelperDAO.almacenaEntidad(usuario3);
	    HelperDAO.almacenaEntidad(usuario4);
	    HelperDAO.almacenaEntidad(usuario5);

	    HelperDAO.eliminaEntidad(usuario2);
	    HelperDAO.eliminaEntidad(usuario5);

        T t = (T) HelperDAO.getEntidad(usuario1.getId(), Usuario.class);
        System.out.println("\n" + t.toString());
        
	}

}
