package listeners.interceptores;

import java.io.Serializable;

import listeners.modelo.Usuario;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

public class InterceptorAuditoria extends EmptyInterceptor{
	
	@Override
	public boolean onSave(
			Object entity, 
			Serializable id, 
			Object[] state, 
			String[] propertyNames, 
			Type[] types) throws CallbackException{
	
		if(entity instanceof Usuario){
			
		    Usuario usuario = (Usuario)entity;
		    System.out.println("\n===");
		    System.out.print("Se va ha almacenar al Usuario : " + usuario.getNombre() );
		    System.out.println("\n===");
		}
		return false;
	}

	@Override
	public void onDelete(
			Object entity, 
			Serializable id, 
			Object[] state, 
			String[] propertyNames, 
			Type[] types) throws CallbackException{
		
	    if(entity instanceof Usuario){
	        Usuario usuario = (Usuario)entity;
		    System.out.println("\n===");
	        System.out.print("Se va a eliminar al Usuario : " + usuario.getNombre() + ", id=" + id);
		    System.out.println("\n===");
	    }
	}

}
