/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listeners.main;

import listeners.dao.HelperDAO;
import listeners.modelo.Usuario;

public class Main {

	public static <T> void main(String[] args) {

		Usuario usuario1 = new Usuario("usuario 1", "username usuario1",
				"password usuario1");
		Usuario usuario2 = new Usuario("usuario 2", "username usuario2",
				"password usuario2");
		Usuario usuario3 = new Usuario("usuario 3", "username usuario3",
				"password usuario3");
		Usuario usuario4 = new Usuario("usuario 4", "username usuario4",
				"password usuario4");

		// Almacenamos las 4 entidades Usuario
		HelperDAO.almacenaEntidad(usuario1);
		HelperDAO.almacenaEntidad(usuario2);
		HelperDAO.almacenaEntidad(usuario3);
		HelperDAO.almacenaEntidad(usuario4);

		// Recuperamos 2 Usuarios
		HelperDAO.getEntidad(usuario2.getId(), Usuario.class);
		HelperDAO.getEntidad(usuario3.getId(), Usuario.class);

		// Actualizamos 3 Usuarios
		usuario1.setNombre("Nuevo nombre del Usuario1");
		usuario2.setNombre("Nuevo nombre del Usuario2");
		usuario3.setNombre("Nuevo nombre del Usuario3");
		HelperDAO.almacenaEntidad(usuario1);
		HelperDAO.almacenaEntidad(usuario2);
		HelperDAO.almacenaEntidad(usuario3);

		// Eliminamos 2 Usuarios
		HelperDAO.eliminaEntidad(usuario1);
		HelperDAO.eliminaEntidad(usuario4);

	}

}
