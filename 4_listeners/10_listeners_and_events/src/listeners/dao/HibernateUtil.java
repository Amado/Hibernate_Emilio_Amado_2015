/*
 * HibernateUtil.java
 *
 * Creada el 24/07/2010, 01:25:07 PM
 *
 * Clase Java desarrollada por Alex para el blog http://javatutoriales.blogspot.com/ el día 24/07/2010
 *
 * Para informacion sobre el uso de esta clase, asi como bugs, actualizaciones, o mejoras enviar un mail a
 * programadorjavablog@gmail.com
 *
 */

package listeners.dao;

import listeners.eventos.ActualizaUsuarioListener;
import listeners.eventos.CargaUsuarioListener;
import listeners.eventos.PostEliminaUsuarioListener;
import listeners.eventos.PreEliminaUsuarioListener;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.event.EventListeners;
import org.hibernate.event.PostDeleteEventListener;
import org.hibernate.event.PostLoadEvent;
import org.hibernate.event.PostLoadEventListener;
import org.hibernate.event.PreDeleteEventListener;
import org.hibernate.event.PreUpdateEventListener;

public class HibernateUtil{
	
    private static final SessionFactory sessionFactory;

    static
    {
        try
        {
            Configuration cfg = new AnnotationConfiguration();
            EventListeners eventListeners = cfg.getEventListeners();
            
            eventListeners.setPostLoadEventListeners (new PostLoadEventListener[]{
            		new CargaUsuarioListener()});
            
            eventListeners.setPreDeleteEventListeners(new PreDeleteEventListener[]{
            		new PreEliminaUsuarioListener()});
            
            eventListeners.setPostDeleteEventListeners(new PostDeleteEventListener[]{
            		new PostEliminaUsuarioListener()});
            
            eventListeners.setPreUpdateEventListeners(new PreUpdateEventListener[]{
            		new ActualizaUsuarioListener()});
            
            sessionFactory = cfg.configure().buildSessionFactory();
        }
        catch (HibernateException he)
        {
            System.err.println("Ocurrió un error en la inicialización de la SessionFactory: " + he);
            throw new ExceptionInInitializerError(he);
        }
    }

    public static SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }
	
}
