package listeners.eventos;

import listeners.modelo.Usuario;

import org.hibernate.event.PreUpdateEvent;
import org.hibernate.event.PreUpdateEventListener;

public class ActualizaUsuarioListener implements PreUpdateEventListener{
    public boolean onPreUpdate(PreUpdateEvent preUploadEvent){
        Object entidad = preUploadEvent.getEntity();
        if(entidad instanceof Usuario){
            Usuario usuario = (Usuario)entidad;
		    System.out.println("\n===");
            System.out.print("Se va a actualizar al usuario : " + usuario.getUsername() + " id(" + preUploadEvent.getId() + ")");
		    System.out.println("\n===");
        }
        return false;
    }
}
