package listeners.eventos;

import listeners.modelo.Usuario;

import org.hibernate.event.PostLoadEvent;
import org.hibernate.event.PostLoadEventListener;

public class CargaUsuarioListener implements PostLoadEventListener {
	@Override
	public void onPostLoad(PostLoadEvent postLoadEvent) {

		Object entidad = postLoadEvent.getEntity();
		if (entidad instanceof Usuario) {
			Usuario usuario = (Usuario) entidad;
		    System.out.println("===");
			System.out.print("Se ha cargado el usuario : " + usuario.getUsername() + " id(" + postLoadEvent.getId() + ")");
		    System.out.println("\n===\n");
		}
	}
}
