package listeners.eventos;

import listeners.modelo.Usuario;

import org.hibernate.event.PostDeleteEvent;
import org.hibernate.event.PostDeleteEventListener;

public class PostEliminaUsuarioListener implements PostDeleteEventListener{
	@Override
	public void onPostDelete(PostDeleteEvent postDeleteEvent) {
        Object entidad = postDeleteEvent.getEntity();
        if (entidad instanceof Usuario){
            Usuario usuario = (Usuario) entidad;
		    System.out.println("\n===");
            System.out.print("Se ha eliminado al Usuario : " + usuario.getUsername() + " id(" + postDeleteEvent.getId() + ")");
		    System.out.println("\n===");
        }
	}
}
