package listeners.eventos;

import listeners.modelo.Usuario;

import org.hibernate.event.PreDeleteEvent;
import org.hibernate.event.PreDeleteEventListener;

public class PreEliminaUsuarioListener implements PreDeleteEventListener{
	@Override
	public boolean onPreDelete(PreDeleteEvent preDeleteEvent){
	    Object entidad = preDeleteEvent.getEntity();
	    if (entidad instanceof Usuario){
	        Usuario usuario = (Usuario) entidad;
		    System.out.println("\n===");
	        System.out.print("Se eliminará al usuario : " + usuario.getUsername() + " id(" + preDeleteEvent.getId() + ")");
		    System.out.println("\n===");
	    }
	    return false;
	}
}
