/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`hibernatehql` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hibernatehql`;


/*Estructura de la tabla `direcciones` */

DROP TABLE IF EXISTS `direcciones`;

CREATE TABLE `direcciones` (                 
               `id` BIGINT(20) NOT NULL AUTO_INCREMENT,   
               `calle` VARCHAR(255) DEFAULT NULL,         
               `codigoPostal` VARCHAR(255) DEFAULT NULL,  
               PRIMARY KEY (`id`)                         
             ) ENGINE=INNODB DEFAULT CHARSET=latin1;    


/*Datos de la tabla `direcciones` */

insert  into `direcciones`(`id`,`calle`,`codigoPostal`) values (1,'Calle1','12345'),(2,'Calle2','54321');


/*Estructura de la tabla `permisos` */

DROP TABLE IF EXISTS `permisos`;

CREATE TABLE `permisos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estatus` int(11) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Datos de la tabla `permisos` */

insert  into `permisos`(`id`,`estatus`,`nombre`) values (1,0,'Lectura Archivos'),(2,0,'Creacion Archivos'),(3,1,'Eliminacion Archivos'),(4,1,'Moficacion Archivos'),(5,2,'Sin Permisos');

/*Estructura de la tabla `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `direccion_id` BIGINT(20) DEFAULT NULL,      
  PRIMARY KEY (`id`),                                                                         
  KEY `FK_DIRECCIONES` (`direccion_id`),                                                    
  CONSTRAINT `FK_DIRECCIONES` FOREIGN KEY (`direccion_id`) REFERENCES `direcciones` (`id`)  
  ) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;          
  

/*Datos de la tabla `usuarios` */

insert  into `usuarios`(`id`,`nombre`,`password`,`username`,`direccion_id`) values (1,'Usuario 1','abcdefg','usr001',1),(2,'Usuario1','hijklm','usr456',2),(3,'Usuario3','alex','alex',null);

/*Estructura de la tabla `usuarios_permisos` */

DROP TABLE IF EXISTS `usuarios_permisos`;

CREATE TABLE `usuarios_permisos` (
  `usuarios_id` bigint(20) NOT NULL,
  `permisos_id` bigint(20) NOT NULL,
  UNIQUE KEY `permisos_id` (`permisos_id`),
  KEY `FK_USUARIOS` (`usuarios_id`),
  KEY `FK_PERMISOS` (`permisos_id`),
  CONSTRAINT `FK_PERMISOS` FOREIGN KEY (`permisos_id`) REFERENCES `permisos` (`id`),
  CONSTRAINT `FK_USUARIOS` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Datos de la tabla `usuarios_permisos` */

INSERT  INTO `usuarios_permisos`(`usuarios_id`,`permisos_id`) VALUES (1,1),(1,2),(2,3),(2,4),(3,5);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
