/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package parametros.main;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;

import parametros.dao.HelperDAO;
import parametros.dao.UsuariosDAO;
import parametros.modelo.Compra;
import parametros.modelo.Direccion;
import parametros.modelo.Producto;
import parametros.modelo.Usuario;

/**
 * 
 * @author Alex
 */
public class Main {

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		
		creaUsuarios();
		
		buscaUsuario();
		
		creaCompras();
		
		buscaUsuariosProductosInactivos();
		
		imprimeEntidad(new Long(2), Usuario.class);
		
		imprimeListaEntidades(Producto.class);
		
	}

	private static void creaUsuarios() {
		System.out.println("\n=============");
		System.out.println("CREA USUARIOS\n");

		HelperDAO.almacenaEntidad(new Usuario("Usuario de Prueba numero 1",
				"estudioso", "desvelado"));
		HelperDAO.almacenaEntidad(new Usuario("Usuario de Prueba numero 2",
				"caperucita", "loboFeroz"));
		HelperDAO.almacenaEntidad(new Usuario("Usuario de Prueba numero 3",
				"empleado", "infelicidad"));
		HelperDAO.almacenaEntidad(new Usuario("Usuario de Prueba numero 4",
				"usuarioComun", "password"));

	}

	/**
	 * Conecta con el m�todo HelperDAO.getEntidad() para imprimir el objeto recuperado
	 * por esta, para ello debe usar los mismos args que el m�todo HelperDAO.getEntidad()
	 * 
	 * Ejemplo de llamada de imprimeEntidad() :
	 * 
	 * 		imprimeEntidad(new Long(2), Usuario.class);
	 * 
	 * @param sId Objeto serializable, en este caso un Long que representa la id
	 * @param claseEntidad objeto que representa la clase-entidad a buscar 
	 */
	private static <T> void imprimeEntidad(Serializable sId, Class<T> claseEntidad) {
		
		String tipoEntidad = claseEntidad.getSimpleName();
		System.out.println("\n=======================");
		System.out.println("IMPRIME ENTIDAD DE TIPO " + tipoEntidad + "\n");
		
		T entidad = HelperDAO.getEntidad(sId, claseEntidad);
		
		System.out.println("\n" + entidad.toString());

	}
	
	/**
	 * Conecta con el m�todo HelperDAO.getListaEntidades() para obtener e imprimir
	 * la lista de entidades almacenadas en la db , para ello debe usar los 
	 * mismos args que el m�todo HelperDAO.getListaEntidades()
	 * 
	 * Ejemplo de llamada de imprimeEntidad() :
	 * 
	 * 		imprimeListaEntidades(Usuario.class);
	 * 
	 * @param sId Objeto serializable, en este caso un Long que representa la id
	 * @param claseEntidad objeto que representa la clase-entidad a buscar 
	 */
	private static <T> void imprimeListaEntidades(Class<T> claseEntidad){
		
		String tipoEntidad = claseEntidad.getSimpleName();
		System.out.println("\n=====================================");
		System.out.println("IMPRIME LA LISTA DE ENTIDADES DE TIPO " + tipoEntidad + "\n");
		
		List<T> listaEntides = HelperDAO.getListaEntidades(claseEntidad);
		System.out.println();
		for(T t :listaEntides) System.out.println(t.toString());
	}

	private static void buscaUsuario() {
		System.out.println("\n=============");
		System.out.println("BUSCA USUARIO\n");

		Usuario usuario = null;
		UsuariosDAO usuariosDAO = new UsuariosDAO();

		// usuario = usuariosDAO.getUsuario_by_username_password("caperucita",
		// "loboFeroz");

		usuario = usuariosDAO.getUsuario_by_id(2);

		if (usuario == null) {
			System.out.println("\nNo se encontró al usuario");
		} else {
			System.out.println("\nEl usuario es: " + usuario.getNombre());
		}

	}

	private static void creaCompras() {
		System.out.println("\n============");
		System.out.println("CREA COMPRAS\n");

		Producto libros = new Producto("Libro", "ABC123456", 120.0F);
		Producto iPads = new Producto("iPad", "RAF755576", 1315.45F);
		Producto televisor = new Producto("T.V.", "AOF765984", 379.64F);
		Producto postales = new Producto("Postal", "ELF", 15.65F);
		Producto juegos = new Producto("Videojuego", "MEN", 158.24F);

		libros.setEstatus(Producto.Estatus.INACTIVO);
		postales.setEstatus(Producto.Estatus.INACTIVO);

		Usuario usuario1 = new Usuario("Usuario de Prueba numero 1",
				"estudioso", "desvelado");
		usuario1.setDireccion(new Direccion("calle principal", "12345"));

		Usuario usuario2 = new Usuario("Usuario de Prueba numero 2",
				"caperucita", "loboFeroz");
		usuario2.setDireccion(new Direccion("primera avenida", "AVR-175"));

		Usuario usuario3 = new Usuario("Usuario de Prueba numero 3",
				"empleado", "infelicidad");
		usuario3.setDireccion(new Direccion("puesta del sol", "12345"));

		Usuario usuario4 = new Usuario("Usuario de Prueba numero 4",
				"usuarioComun", "password");
		usuario4.setDireccion(new Direccion("Este 145", null));

		Compra compraUsuario1 = new Compra();
		compraUsuario1.addProducto(libros);
		usuario1.addCompra(compraUsuario1);

		compraUsuario1 = new Compra();
		compraUsuario1.addProducto(televisor);
		compraUsuario1.addProducto(juegos);
		usuario1.addCompra(compraUsuario1);

		Compra compraUsuario2 = new Compra();
		compraUsuario2.addProducto(iPads);
		compraUsuario2.addProducto(televisor);
		compraUsuario2.addProducto(juegos);
		usuario2.addCompra(compraUsuario2);

		Compra compraUsuario3 = new Compra();
		compraUsuario3.addProducto(iPads);
		compraUsuario3.addProducto(televisor);
		usuario3.addCompra(compraUsuario3);

		compraUsuario3 = new Compra();
		compraUsuario3.addProducto(postales);
		compraUsuario3.addProducto(juegos);
		usuario3.addCompra(compraUsuario3);

		Compra compraUsuario4 = new Compra();
		compraUsuario4.addProducto(libros);
		usuario4.addCompra(compraUsuario4);

		HelperDAO.almacenaEntidad(libros);
		HelperDAO.almacenaEntidad(iPads);
		HelperDAO.almacenaEntidad(televisor);
		HelperDAO.almacenaEntidad(postales);
		HelperDAO.almacenaEntidad(juegos);

		HelperDAO.almacenaEntidad(usuario1);
		HelperDAO.almacenaEntidad(usuario2);
		HelperDAO.almacenaEntidad(usuario3);
		HelperDAO.almacenaEntidad(usuario4);

	}

	private static void buscaUsuariosProductosInactivos() {
		System.out.println("\n==================================");
		System.out.println("BUSCA USUARIOS PRODUCTOS INACTIVOS\n");

		List<Usuario> listaUsuarios = new UsuariosDAO()
				.getUsuariosConComprasInactivas("12345");

		for (int i = 0; i < listaUsuarios.size(); i++) {
			Usuario usuarioActual = listaUsuarios.get(i);

			System.out.println("\nUsuario: " + usuarioActual.getNombre());

			List<Compra> listaCompras = usuarioActual.getCompras();

			for (int numeroCompra = 0; numeroCompra < listaCompras.size(); numeroCompra++) {
				List<Producto> listaProductos = listaCompras.get(numeroCompra)
						.getProductos();

				for (int numeroProducto = 0; numeroProducto < listaProductos
						.size(); numeroProducto++) {
					Producto producto = listaProductos.get(numeroProducto);

					System.out.println("\t" + producto.getNombre());
				}
			}
		}
	}
	
}
