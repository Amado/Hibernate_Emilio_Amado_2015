/*
 * UsuariosDAO.java
 *
 * Creada el 24/07/2010, 01:22:04 PM
 *
 * Clase Java desarrollada por Alex para el blog http://javatutoriales.blogspot.com/ el día 24/07/2010
 *
 * Para informacion sobre el uso de esta clase, asi como bugs, actualizaciones, o mejoras enviar un mail a
 * programadorjavablog@gmail.com
 *
 */
package parametros.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import parametros.modelo.Producto;
import parametros.modelo.Usuario;

/**
 * @author Alex
 * @version 1.0
 * @author-mail programadorjavablog@gmail.com
 * @date 24/07/2010
 */
public class UsuariosDAO extends HelperDAO
{
	
    public Usuario getUsuario_by_id(long id) throws HibernateException
    {
        Usuario usuario = null;

        try
        {
            iniciaOperacion();
            Query query = getSession().createQuery("FROM Usuario u WHERE u. id = :idUsuario");
            query.setParameter("idUsuario", id);

            usuario = (Usuario)query.uniqueResult();
        }
        catch (HibernateException he)
        {
            manejaExcepcion(he);
        }
        finally
        {
            terminaOperacion();
        }


        return usuario;
    }
	
    public Usuario getUsuario_by_username_password(String username, String password) throws HibernateException
    {
        Usuario usuario = null;

        try
        {
            iniciaOperacion();
            Query query = getSession().createQuery("FROM Usuario u WHERE u. username = :nombreUsuario AND u. password = :password");
            query.setParameter("nombreUsuario", username);
            query.setParameter("password", password);

            usuario = (Usuario)query.uniqueResult();
        }
        catch (HibernateException he)
        {
            manejaExcepcion(he);
        }
        finally
        {
            terminaOperacion();
        }


        return usuario;
    }

    public List<Usuario> getUsuariosConComprasInactivas(String codigoPostal) throws HibernateException
    {
        List<Usuario> listaUsuarios = null;

        try
        {
            iniciaOperacion();

            Query query = getSession().createQuery("SELECT u FROM Usuario u JOIN FETCH u.compras c JOIN c.productos p WHERE p.estatus = ? AND u.direccion.codigoPostal = ?");
            query.setParameter(0, Producto.Estatus.INACTIVO);
            query.setParameter(1, codigoPostal);
            
            listaUsuarios = query.list();

        }catch(HibernateException he)
        {
            he.printStackTrace();
            manejaExcepcion(he);
        }finally
        {
            terminaOperacion();
        }

        return listaUsuarios;
    }
}
