package hql.dao;

import java.util.List;
import java.util.Map;

import hql.modelo.Usuario;
import hql.query_classes.Get_nombreUsuario_direccionUsuario_numeroPermisosUsuario;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class UsuarioDAO {

	private static Session session;
	private static Transaction tx;

	//=========================================================================
	public static long insertTelevidente(Usuario u) throws HibernateException {
		long id;

		try {
			iniciaOperacion();
			id = (Long) session.save(u);
			tx.commit();
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
			session.close();
		}

		return id;
	}

	//=========================================================================
	public static void deleteTelevidente(Usuario u) throws HibernateException {

		try {
			iniciaOperacion();
			session.delete(u);
			tx.commit();
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
			session.close();
		}
	}

	//==========================================================================
	public static List<Usuario> obtenListaUsuarios() throws HibernateException {
		List<Usuario> listausuarios = null;

		try {
			iniciaOperacion();
			listausuarios = session.createQuery("from Usuario").list();
			tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally {
			session.close();
		}

		return listausuarios;
	}
	
	//==================================================================
	public static List<String> obtenNombres() throws HibernateException{
		
		List<String> listaResultados = null;
		
		try{
			iniciaOperacion();
//			Query query = session.createQuery("SELECT u.nombre FROM Usuario as u");
			Query query = session.createQuery("SELECT nombre FROM Usuario");
			listaResultados = query.list();
			tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally{
			session.close();
		}
		return listaResultados;
	}
	
	
	//=================================================================
	//Formamos una colecci�n de arrays con los datos deseados de la db
	//=================================================================
	public static List<Object[]> obtenNombresYPasswordsAsObjects() throws HibernateException{
		
		List<Object[]> listaResultados = null;
		
		try{
	        iniciaOperacion();
	        Query query = session.createQuery("SELECT u.nombre, u.password FROM Usuario as u ");
	        listaResultados = query.list();
	        tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally{
			session.close();
		}
		return listaResultados;
	}
	
	
	//=================================================================
	//Formamos una colecci�n de listas con los datos deseados de la db
	//=================================================================
	public static List<List<String>> obtenNombresYPasswordsAsList() throws HibernateException{
		
		List<List<String>> listaResultados = null;
		try{
			iniciaOperacion();
		    Query query = session.createQuery("SELECT new list(u.nombre, u.password) FROM Usuario as u ");
		    listaResultados = query.list();
		    tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally{
			session.close();
		}
		return listaResultados;
	}
	
	
	//=================================================================
	//Formamos una colecci�n de Maps con los datos deseados de la db
	//=================================================================
	public static List<Map> obtenNombresYPasswordsAsmap() throws HibernateException{
		
		List<Map> listaResultados = null;
		
		try{
			iniciaOperacion();
		    Query query = session.createQuery(
		    		"SELECT new map(u.id as identificador, u.nombre as nombre, u.password as pass) FROM Usuario as u");
		    listaResultados = query.list();
		    tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally{
			session.close();
		}
		return listaResultados;
	}
	
	
	//=======================================================================================
	public static List<Get_nombreUsuario_direccionUsuario_numeroPermisosUsuario> obtenUsuariosDireccion() throws HibernateException {
		
		List<Get_nombreUsuario_direccionUsuario_numeroPermisosUsuario> listaResultados = null;
		
		try{
			iniciaOperacion();
			Query query = session.createQuery(
					  "SELECT NEW hql.query_classes.Get_nombreUsuario_direccionUsuario_numeroPermisosUsuario"
					+ "(u.nombre, d.calle, d.codigoPostal, COUNT(p)) "
					+ "FROM Usuario u left outer join u.direccion as d left outer join u.permisos as p "
					+ "GROUP BY u.nombre");
		    listaResultados = query.list();
		    tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally{
			session.close();
		}
		return listaResultados;
	}
	
	
	//====================================================================
	public static Usuario obtenUsaurio_usr456() throws HibernateException{
		Usuario result = null;
		
		try{
			iniciaOperacion();
			Query query = session.createQuery(
					  "FROM Usuario u WHERE u.username='usr456'");
			result = (Usuario) query.uniqueResult();
			tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally{
			session.close();
		}
		return result;
	}
	
	//====================================================================================
	public static List<Usuario> obtenUsuario_codPostal_123456() throws HibernateException{
				
		List<Usuario> result = null;
		
		try{
			iniciaOperacion();
			Query query = session.createQuery(
					  "FROM Usuario u WHERE u.direccion.codigoPostal = '12345'");
			result = query.list();
			tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally{
			session.close();
		}
		return result;			
	}
	
	//=====================================================================================
	public static List<Usuario> obtenUsuario_direccion_notNull() throws HibernateException{
		List<Usuario> result = null;
		
		try{
			iniciaOperacion();
			Query query = session.createQuery(
					  "FROM Usuario u WHERE u.direccion IS NOT NULL");

			result = query.list();
			tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally{
			session.close();
		}
		return result;	
	}
	
	//=================================================================================
	public static List<Usuario> obtenUsuario_permisos_eq_1() throws HibernateException{
		List<Usuario> result = null;
		
		try{
			iniciaOperacion();
			Query query = session.createQuery(
					    "FROM Usuario u inner join u.permisos as p WHERE p in (SELECT p FROM Permiso p WHERE p.estatus = 1)");

			result = query.list();
			tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally{
			session.close();
		}
		return result;
	}
	
	//=================================================================================
	public static long obtenNumero_resultados_Usuario_join_Permiso() throws HibernateException{
		long result;
		
		try{
			iniciaOperacion();
//			Query query = session.createQuery(
//					    "SELECT COUNT(*) FROM Usuario u, Permiso p");
//
//			result = (long) query.uniqueResult();
			
			result = ((Long) session.createQuery("SELECT COUNT(*) FROM Usuario u, Permiso p").iterate().next()).intValue();
			
			tx.commit();
		} catch (HibernateException he) { 
            manejaExcepcion(he); 
            throw he; 
		} finally{
			session.close();
		}
		return result;
	}

	
	//===============================================================
	private static void iniciaOperacion() throws HibernateException {

		session = HibernateUtil.getSessionFactory().openSession(); 
		tx = session.beginTransaction(); 
	}

	//========================================================
	private static void manejaExcepcion(HibernateException he)
			throws HibernateException {
		tx.rollback();
		throw new HibernateException(
				"Ocurrió un error en la capa de acceso a datos", he);
	}

}
