package hql.dao;

import hql.modelo.Permiso;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class PermisoDAO {

	private static Session session;
	private static Transaction tx;

	// =======================================================================
	public static List<Permiso> obtenPermisos_idgt2_and_idgt_or_eq5()
			throws HibernateException {
		List<Permiso> result = null;

		try {
			iniciaOperacion();
			Query query = session
					.createQuery("FROM Permiso p WHERE p.id> 2 AND p.id <= 5");
			result = query.list();
			tx.commit();
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
			session.close();
		}
		return result;
	}

	// ========================================================================================
	public static List<Permiso> obtenPermisos_orderBy_nombre_desc()
			throws HibernateException {
		List<Permiso> result = null;

		try {
			iniciaOperacion();
			Query query = session
					.createQuery("FROM Permiso p ORDER BY p.nombre desc");
			result = query.list();
			tx.commit();
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
			session.close();
		}
		return result;
	}

	// ========================================================================================
	public static List<Object[]> obten_cuenta_permisos()
			throws HibernateException {
		List<Object[]> result = null;

		try {
			iniciaOperacion();
			Query query = session
					.createQuery("SELECT p.estatus, COUNT(p.estatus) FROM Permiso p GROUP BY p.estatus");
			result = query.list();
			tx.commit();
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
			session.close();
		}
		return result;
	}

	// ===============================================================
	private static void iniciaOperacion() throws HibernateException {

		session = HibernateUtil.getSessionFactory().openSession();
		tx = session.beginTransaction();
	}

	// ========================================================
	private static void manejaExcepcion(HibernateException he)
			throws HibernateException {
		tx.rollback();
		throw new HibernateException(
				"Ocurrió un error en la capa de acceso a datos", he);
	}

}
