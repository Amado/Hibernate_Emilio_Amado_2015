/*
 * Usuario.java
 *
 * Creada el 4/09/2009, 10:57:26 PM
 *
 * Clase Java desarrollada por Programador Java para el blog http://javatutoriales.blogspot.com/ el d�a 4/09/2009
 *
 * Para informaci�n sobre el uso de esta clase, as� como bugs, actualizaciones, o mejoras env�ar un mail a  
 * programadorjavablog@gmail.com
 */


package hql.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Programador Java
 * @version 1.0
 * @author-mail programadorjavablog@gmail.com
 * @date 4/09/2009
 * @proyect HQL Parte 2
 */
@Entity
@Table(name="usuarios")
public class Usuario implements Serializable
{
    @Id
//    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    private String nombre;
    private String username;
    private String password;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Permiso> permisos = new ArrayList<Permiso>();

    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    private Direccion direccion;

    public long getId()
    {
        return id;
    }

    protected void setId(long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public List<Permiso> getPermisos()
    {
        return permisos;
    }

    public void setPermisos(List<Permiso> permisos)
    {
        this.permisos = permisos;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public Direccion getDireccion()
    {
        return direccion;
    }

    public void setDireccion(Direccion direccion)
    {
        this.direccion = direccion;
        direccion.setUsuario(this);
    }

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nombre=" + nombre + ", username="
				+ username + ", password=" + password + ", permisos="
				+ permisos.size() + ", direccion=" + direccion + "]";
	}

}