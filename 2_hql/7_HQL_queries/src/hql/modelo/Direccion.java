/*
 * Direccion.java
 *
 * Creada el 8/09/2009, 09:17:19 PM
 *
 * Clase Java desarrollada por Programador Java para el blog http://javatutoriales.blogspot.com/ el d�a 8/09/2009
 *
 * Para informaci�n sobre el uso de esta clase, as� como bugs, actualizaciones, o mejoras env�ar un mail a  
 * programadorjavablog@gmail.com
 */


package hql.modelo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Programador Java
 * @version 1.0
 * @author-mail programadorjavablog@gmail.com
 * @date 8/09/2009
 * @proyect HQL Parte 2
 */
@Entity
@Table(name="direcciones")
public class Direccion implements Serializable
{
    @Id
//    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    private String calle;
    private String codigoPostal;

    @OneToOne(mappedBy="direccion")
    private Usuario usuario;

    public Direccion()
    {
    }

    public String getCalle()
    {
        return calle;
    }

    public void setCalle(String calle)
    {
        this.calle = calle;
    }

    public String getCodigoPostal()
    {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal)
    {
        this.codigoPostal = codigoPostal;
    }

    public long getId()
    {
        return id;
    }

    protected void setId(long id)
    {
        this.id = id;
    }

    public Usuario getUsuario()
    {
        return usuario;
    }

    public void setUsuario(Usuario usuario)
    {
        this.usuario = usuario;
    }

	@Override
	public String toString() {
		return "Direccion [id=" + id + ", calle=" + calle + ", codigoPostal="
				+ codigoPostal + "]";
	}
	
	

}