/*
 * Permiso.java
 *
 * Creada el 1/09/2009, 11:17:01 PM
 *
 * Clase Java desarrollada por Programador Java para el blog http://javatutoriales.blogspot.com/ el d�a 1/09/2009
 *
 * Para informaci�n sobre el uso de esta clase, as� como bugs, actualizaciones, o mejoras env�ar un mail a  
 * programadorjavablog@gmail.com
 */


package hql.modelo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Programador Java
 * @version 1.0
 * @author-mail programadorjavablog@gmail.com
 * @date 1/09/2009
 * @proyect HQL Parte 2
 */
@Entity
@Table(name="permisos")
public class Permiso implements Serializable
{
    public enum Estatus {PENDIENTE, ACTIVO, INACTIVO};

    @Id
//    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    private String nombre;
    private Estatus estatus /*= Estatus.PENDIENTE*/;

    public Permiso()
    {
    }

    public Estatus getEstatus()
    {
        return estatus;
    }

    public void setEstatus(Estatus estatus)
    {
        this.estatus = estatus;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

	@Override
	public String toString() {
		return "Permiso [id=" + id + ", nombre=" + nombre + ", estatus="
				+ estatus + "]";
	}
    
}