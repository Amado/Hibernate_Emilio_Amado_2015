/*
 * Main.java
 *
 * Creada el 31/08/2009, 11:35:05 PM
 *
 * Clase Java desarrollada por Programador Java para el blog http://javatutoriales.blogspot.com/ el d�a 31/08/2009
 *
 * Para informaci�n sobre el uso de esta clase, as� como bugs, actualizaciones, o mejoras env�ar un mail a  
 * programadorjavablog@gmail.com
 */
package hql;

import hql.dao.HibernateUtil;
import hql.dao.PermisoDAO;
import hql.dao.UsuarioDAO;
import hql.modelo.Permiso;
import hql.modelo.Usuario;
import hql.query_classes.Get_nombreUsuario_direccionUsuario_numeroPermisosUsuario;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.type.Type;

/**
 *
 * @author Programador Java
 * @version 1.0
 * @author-mail programadorjavablog@gmail.com
 * @date 31/08/2009
 * @proyect HQL Parte 2
 */
public class Main {
	
	public static void main(String[] args) {
		
		imprimeListaUsuarios();
		
		imprimeNombresUsuarios();
		
		imprimeNombresYPasswordsAsObjects();
		
		imprimeNombresYPasswordsAsList();
		
		imprimeNombresYPasswordsAsMap();
		
		imprimeUsuariosDireccion();
		
		imprimeUsaurio_usr456();
		
		imprimePermisos_idgt2_and_idgt_or_eq5();
		
		imprimeUsuario_codPostal_12345();
		
		imprimeUsuario_direccion_notNull();
		
		imprimePermisos_orderBy_nombre_desc();
		
		imprime_cuenta_permisos();
		
		imprimeUsuario_permisos_eq_1();
		
		imprimeNumero_resultados_Usuario_join_Permiso();

	}
	
	private static void imprimeListaUsuarios(){		
		System.out.println("\n==================");
		System.out.println("TODOS LOS USUARIOS\n");
		List<Usuario> todosUsuarios = UsuarioDAO.obtenListaUsuarios();

		System.out.println();
		for(Usuario u : todosUsuarios) System.out.println(u.toString());
		System.out.println();
	}
	
	private static void imprimeNombresUsuarios(){		
		System.out.println("\n======================");
		System.out.println("NOMBRE DE LOS USUARIOS\n");
		List<String> nombreUsuarios = UsuarioDAO.obtenNombres();

		System.out.println();
		for (int i = 0; i < nombreUsuarios.size(); i++) {
			System.out.println("Nombre " + i + ": " + nombreUsuarios.get(i));
		}
		System.out.println();
	}
	
	private static void imprimeNombresYPasswordsAsObjects(){		
		System.out.println("\n===============================");
		System.out.println("NOMBRES Y PASSWORDS COMO ARRAYS\n");
		List<Object[]> listaResultados = UsuarioDAO.obtenNombresYPasswordsAsObjects();
		
		/////////////////////////////////////////////////////
		//OBTENER NOMBRES DE COLUMNAS DE UNA ENTIDAD EN LA DB
		System.out.println();
		AbstractEntityPersister aep = (AbstractEntityPersister) HibernateUtil.getSessionFactory().getClassMetadata(Usuario.class);
        String[] properties = aep.getPropertyNames();
        
        for(int nameIndex=0;nameIndex!=properties.length;nameIndex++){
        	System.out.print("PropertyName: " + properties[nameIndex] + " -> ");
        	String[] columns = aep.getPropertyColumnNames(nameIndex);
        	for(int columnsIndex=0;columnsIndex!=columns.length;columnsIndex++){
        		System.out.print(" ColumnName: " + columns[columnsIndex]);
        	}
        	System.out.println();
        }
        /////////////////////////////////////////////////////

		System.out.println();
		
		String line = "-----------------------------";
		
		System.out.println(
				"+" + String.format("%.11s", line) +
				"+" +String.format("%.11s", line) +
				"+" +String.format("%.11s", line) + "+");
		
		System.out.println(
				String.format("%1$-12s", "|posicion") +
				String.format("%1$-12s", "|nombre") +
				String.format("%1$-12s", "|password") + "|");
		
		System.out.println(
				"+" + String.format("%.11s", line) +
				"+" +String.format("%.11s", line) +
				"+" +String.format("%.11s", line) + "+");
		
		for (int i = 0; i < listaResultados.size(); i++) {
//            System.out.println("Nombre " + i + ": " + listaResultados.get(i)[0] + ", password: " + listaResultados.get(i)[1]);
            
            System.out.println(
    				String.format("%-12s", "|" + i) +
    				String.format("%-12s", "|" + listaResultados.get(i)[0]) +
    				String.format("%-12s", "|" + listaResultados.get(i)[1]) + "|");
        }
		
		System.out.println(
				"+" + String.format("%.11s", line) +
				"+" +String.format("%.11s", line) +
				"+" +String.format("%.11s", line) + "+");
		
		System.out.println();
	}
	
	
	private static void imprimeNombresYPasswordsAsList(){		
		System.out.println("\n===============================");
		System.out.println("NOMBRES Y PASSWORDS COMO LISTAS\n");
		List<List<String>> listaResultados = UsuarioDAO.obtenNombresYPasswordsAsList();

		System.out.println();
		for (int i = 0; i < listaResultados.size(); i++) {
	        System.out.println("Nombre " + i + ": " + listaResultados.get(i).get(0) + ", password: " + listaResultados.get(i).get(1));
	    }
		System.out.println();
	}
	
	
	private static void imprimeNombresYPasswordsAsMap(){		
		System.out.println("\n=============================");
		System.out.println("NOMBRES Y PASSWORDS COMO MAPS\n");
		List<Map> listaResultados = UsuarioDAO.obtenNombresYPasswordsAsmap();

		System.out.println();
		for (int i = 0; i < listaResultados.size(); i++) {
			
			Map mapActual = listaResultados.get(i);
			Iterator<Entry> it = mapActual.entrySet().iterator();
			
			System.out.println("Datos del mapa " + i);
			
			while(it.hasNext()){
				Entry elemento = it.next(); //con Entry recogemos el siguiente elemento del iterador con su Key y Value
				String k = "" + elemento.getKey(); //obtenemos la key
				String v = "" + elemento.getValue(); //obtenemos el Value
				
				System.out.println("\tLlave: " + k + ", valor: " + v);
			}
		}
		System.out.println();
	}
	
	
	private static void imprimeUsuariosDireccion(){		
		System.out.println("\n======================");
		System.out.println("USUARIOS Y DIRECCIONES\n");
		List<Get_nombreUsuario_direccionUsuario_numeroPermisosUsuario> listaResultados = UsuarioDAO.obtenUsuariosDireccion();

		System.out.println();
		for (int i = 0; i < listaResultados.size(); i++){
			
	        Get_nombreUsuario_direccionUsuario_numeroPermisosUsuario usuarioDireccion = listaResultados.get(i);
//	        System.out.println("->" + usuarioDireccion.getNombre() + ", permisos: " + usuarioDireccion.getNumeroPermisos());	        
	        System.out.println(usuarioDireccion.toString());
	    }
		System.out.println();
	}
	
	private static void imprimeUsaurio_usr456(){		
		System.out.println("\n==============");
		System.out.println("Usaurio_usr456\n");
		Usuario u = UsuarioDAO.obtenUsaurio_usr456();

		System.out.println("\n" + u.toString());
		
		System.out.println();
	}
	
	private static void imprimePermisos_idgt2_and_idgt_or_eq5(){		
		System.out.println("\n=======================");
		System.out.println("Permisos_id>2_and_id<=5\n");
		List<Permiso> permisosList = PermisoDAO.obtenPermisos_idgt2_and_idgt_or_eq5();

		System.out.println();
		for(Permiso p : permisosList)System.out.println(p.toString());
		System.out.println();
	}
	
	private static void imprimeUsuario_codPostal_12345(){		
		System.out.println("\n=======================");
		System.out.println("Usuario_codPostal_12345\n");
		List<Usuario> usuariosList = UsuarioDAO.obtenUsuario_codPostal_123456();

		System.out.println();
		for(Usuario p : usuariosList)System.out.println(p.toString());
		System.out.println();
	}
	
	private static void imprimeUsuario_direccion_notNull(){		
		System.out.println("\n=========================");
		System.out.println("Usuario_direccion_notNull\n");
		List<Usuario> usuariosList = UsuarioDAO.obtenUsuario_direccion_notNull();

		System.out.println();
		for(Usuario p : usuariosList)System.out.println(p.toString());
		System.out.println();
	}
	
	private static void imprimePermisos_orderBy_nombre_desc(){		
		System.out.println("\n=========================");
		System.out.println("Usuario_direccion_notNull\n");
		List<Permiso> resultList = PermisoDAO.obtenPermisos_orderBy_nombre_desc();

		System.out.println();
		for(Permiso p : resultList)System.out.println(p.toString());
		System.out.println();
	}
	
	private static void imprime_cuenta_permisos(){		
		System.out.println("\n=======================");
		System.out.println("Permiso_cuenta_permisos\n");
		List<Object[]> resultList = PermisoDAO.obten_cuenta_permisos();

		System.out.println();
		for(Object[] p : resultList)System.out.println("Estatus : " + p[0] + " (" + p[1] +")");
		System.out.println();
	}
	
	private static void imprimeUsuario_permisos_eq_1(){		
		System.out.println("\n=====================");
		System.out.println("Usuario_permisos_eq_1\n");
		List<Usuario> usuariosList = UsuarioDAO.obtenUsuario_direccion_notNull();

		System.out.println();
		for(Usuario p : usuariosList)System.out.println(p.toString());
		System.out.println();
	}
	
	private static void imprimeNumero_resultados_Usuario_join_Permiso(){		
		System.out.println("\n======================================");
		System.out.println("Numero_resultados_Usuario_join_Permiso\n");
		long result = UsuarioDAO.obtenNumero_resultados_Usuario_join_Permiso();

		System.out.println("\n" + result);
		
		System.out.println();
	}

}
