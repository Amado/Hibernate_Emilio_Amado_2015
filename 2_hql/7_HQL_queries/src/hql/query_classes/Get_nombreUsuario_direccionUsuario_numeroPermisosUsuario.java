/*
 * UsuarioDireccion.java
 *
 * Creada el 4/09/2009, 10:57:26 PM
 *
 * Clase Java desarrollada por Programador Java para el blog http://javatutoriales.blogspot.com/ el d�a 4/09/2009
 *
 * Para informaci�n sobre el uso de esta clase, as� como bugs, actualizaciones, o mejoras env�ar un mail a
 * programadorjavablog@gmail.com
 */
package hql.query_classes;

/**
 * @author Programador Java
 * @version 1.0
 * @author-mail programadorjavablog@gmail.com
 * @date 4/09/2009
 * @proyect HQL Parte 2
 */

/*
 * Podemos crear una clase con el unico objetivo de recuperar
 * solo ciertos datos de la db, representando esos datos
 * en el constructor de la clase, cuyos atts sean los
 * valores que estamos recuperando.
 * 
 * Crearemos una clase �UsuarioDireccion�, que mantendr� el nombre del Usuario, 
 * su calle y c�digo postal, y el n�mero de permisos que tiene
 */
public class Get_nombreUsuario_direccionUsuario_numeroPermisosUsuario
{
    private String nombre;
    private String calle;
    private String codigoPostal;
    private long numeroPermisos;

    public Get_nombreUsuario_direccionUsuario_numeroPermisosUsuario(
    		String nombre, String calle, String codigoPostal, long numeroPermisos)
    {
        this.nombre = nombre;
        this.calle = calle;
        this.codigoPostal = codigoPostal;
        this.numeroPermisos = numeroPermisos;
    }

    
    
    public String getCalle()
    {
        return calle;
    }

    public void setCalle(String calle)
    {
        this.calle = calle;
    }

    public String getCodigoPostal()
    {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal)
    {
        this.codigoPostal = codigoPostal;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public long getNumeroPermisos()
    {
        return numeroPermisos;
    }

    public void setNumeroPermisos(long numeroPermisos)
    {
        this.numeroPermisos = numeroPermisos;
    }



	@Override
	public String toString() {
		return "  -> nombreUsuario_direccionUsuario_numeroPermisosUsuario :\n\t"
				+ "[nombre=" + nombre + ", calle=" + calle
				+ ", codigoPostal=" + codigoPostal + ", numeroPermisos="
				+ numeroPermisos + "]";
	}
    
    
}