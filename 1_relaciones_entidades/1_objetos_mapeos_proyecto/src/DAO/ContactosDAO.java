package DAO;

import java.util.List;

import objetos.Contacto;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import HibernateUtil.HibernateUtil;

public class ContactosDAO {

	private Session sesion;
	private Transaction tx;

	// abre la sesison e inicia una transaccion sobre la sesion
	//obtiene la sesion de la clase HibernateUtil y la inicia con opensession()
	private void iniciaOperacion() throws HibernateException {
		sesion = HibernateUtil.getSessionFactory().openSession();
		tx = sesion.beginTransaction();
	}

	//lanza una excepci�n si algo va mal, en cuyo caso hace un rollback sobre la transaccion
	private void manejaExcepcion(HibernateException he)throws HibernateException {
		tx.rollback();
		throw new HibernateException(
				"Ocurri� un error en la capa de acceso a datos", he);
	}
	
	//realiza un insert sobre la db, metodo "save()"
	public long guardaContacto(Contacto contacto){ 
	    long id = 0;  

	    try { 
	        iniciaOperacion(); 
	        id = (Long)sesion.save(contacto); 
	        tx.commit(); 
	    }
	    catch(HibernateException he){ 
	        manejaExcepcion(he);
	        throw he; 
	    }
	    finally { 
	        sesion.close(); 
	    }  
	    return id; 
	}
	
	//realiza un update sobre la db, metodo "update()"
	public void actualizaContacto(Contacto contacto) throws HibernateException { 
	    try { 
	        iniciaOperacion(); 
	        sesion.update(contacto); 
	        tx.commit(); 
	    }
	    catch (HibernateException he){ 
	        manejaExcepcion(he); 
	        throw he; 
	    }
	    finally{ 
	        sesion.close(); 
	    } 
	}
	
	//realiza un delete sobre la db, metodo "delete()"
	public void eliminaContacto(Contacto contacto) throws HibernateException{ 
	    try  { 
	        iniciaOperacion(); 
	        sesion.delete(contacto); 
	        tx.commit(); 
	    } 
	    catch (HibernateException he) { 
	        manejaExcepcion(he); 
	        throw he; 
	    }
	    finally { 
	        sesion.close(); 
	    } 
	}
	
	//realiza un select de un contacto sobre la db, m�todo "get()"
	//para ello usamos la id del contacto
	public Contacto obtenContacto(long idContacto) throws HibernateException{ 
	    Contacto contacto = null;  

	    try { 
	        iniciaOperacion(); 
	        contacto = (Contacto) sesion.get(Contacto.class, idContacto); 
	    } 
	    finally { 
	        sesion.close(); 
	    }  
	    return contacto; 
	}
	
	//realiza un select de TODOS los contactos sobre la db, m�todos createQuery().list()
	public List<Contacto> obtenListaContactos() throws HibernateException { 
	    List<Contacto> listaContactos = null;  
	    
	    try { 
	        iniciaOperacion(); 
	        listaContactos = sesion.createQuery("from Contacto").list(); 
	    }
	    finally { 
	        sesion.close(); 
	    }  

	    return listaContactos; 
	}

}
