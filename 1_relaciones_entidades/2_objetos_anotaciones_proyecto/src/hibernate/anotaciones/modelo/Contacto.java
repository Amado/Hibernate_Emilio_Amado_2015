/*
 * Contacto.java
 *
 * Creada el 26-ago-2010, 14:33:03
 *
 * Clase Java desarrollada por Alex para el blog http://javatutoriales.blogspot.com/ el día 26-ago-2010
 *
 * Para informacion sobre el uso de esta clase, asi como bugs, actualizaciones, o mejoras enviar un mail a
 * programadorjavablog@gmail.com
 *
 */

package hibernate.anotaciones.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //inidca que la clase es una Entidad
@Table(name="contactos") //hibernate creara para esta Entidad la tabla "contactos" en la db
public class Contacto implements Serializable
{
	
	//PODEMOS USAR LA ANOTACION @Transient para indicar que un att no debe persistir en la db
    @Id //indicamos que el att es el identificador de la Entidad
    @GeneratedValue(strategy=GenerationType.IDENTITY) //el identificador se generar en orden 1,2,3 ...
    private long id;
    private String nombre;
    
    @Column(name="e_mail") //indicamos el nombre que deber� tener la columna en la db, si no, usa el nombre del att
    private String email;
    private String telefono;

    public Contacto()
    {
    }

    public Contacto(String nombre, String email, String telefono)
    {
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public long getId()
    {
        return id;
    }

    private void setId(long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getTelefono()
    {
        return telefono;
    }

    public void setTelefono(String telefono)
    {
        this.telefono = telefono;
    }
}