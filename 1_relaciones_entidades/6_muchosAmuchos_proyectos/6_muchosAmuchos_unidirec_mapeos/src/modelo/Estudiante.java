package modelo;

import java.util.ArrayList;
import java.util.List;

public class Estudiante {
	private long id; 
	private String nombre;
	private List<Materia> materias = new ArrayList<Materia>();

	public Estudiante() {
	}

	public long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	public List<Materia> getMaterias() {
		return materias;
	}

	public void setMaterias(List<Materia> materias) {
		this.materias = materias;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void addMateria(Materia materia) {
		this.materias.add(materia);
	}
}
