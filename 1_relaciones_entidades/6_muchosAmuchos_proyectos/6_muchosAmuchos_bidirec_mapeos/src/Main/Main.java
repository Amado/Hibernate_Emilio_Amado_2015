package Main;

import dao.MateriaDAO;
import dao.EstudianteDAO;
import modelo.Materia;
import modelo.Estudiante;


public class Main {

	public static void main(String[] args) {

	    /* Creamos los objetos */

	    Estudiante estudiante1 = new Estudiante();
	    estudiante1.setNombre("estudiante1");

	    Materia materia1 = new Materia();
	    materia1.setNombre("materia1");
	    Materia materia2 = new Materia();
	    materia2.setNombre("materia2");
	    Materia materia3 = new Materia();
	    materia3.setNombre("materia3");

	    materia1.addEstudiante(estudiante1);
	    materia2.addEstudiante(estudiante1);
	    materia3.addEstudiante(estudiante1);


	    Estudiante estudiante2 = new Estudiante();
	    estudiante2.setNombre("estudiante2");

	    Materia materia4 = new Materia();
	    materia4.setNombre("materia4");
	    Materia materia5 = new Materia();
	    materia5.setNombre("materia5");
	    Materia materia6 = new Materia();
	    materia6.setNombre("materia6");

	    materia4.addEstudiante(estudiante2);
	    materia5.addEstudiante(estudiante2);
	    materia6.addEstudiante(estudiante2);


	    /* Guardamos estudiantes en la base de datos 
	     * Al introducir los estudiantes en la db, las materias 
	     * ser�n tambi�n introducidas en la db en cascada */
	    
	    EstudianteDAO.insertEstudiante(estudiante1);
	    EstudianteDAO.insertEstudiante(estudiante2);
	    
	    
	    /* eliminamos un estudiantes */
	    
	    EstudianteDAO.deleteEstudiante(estudiante1);

	}

}
