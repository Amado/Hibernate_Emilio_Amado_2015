package dao;

import modelo.Materia;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class MateriaDAO {

	private static Session session;

	public static void insertMateria(Materia m) {

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.persist(m);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			System.out.println("Ocurrio un error en la capa de acceso a datos"
					+ e);
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static void deleteMateria(Materia m) {

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(m);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			System.out.print("Ocurrio un error en la capa de acceso a datos"
					+ e);
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}
