package modelo;

import java.util.ArrayList;
import java.util.List;

public class Materia {

	private long id;
	private String nombre;
	private List<Estudiante> estudiantes = new ArrayList<Estudiante>();

	public Materia() {
	}

	public long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Estudiante> getEstudiantes() {
		return estudiantes;
	}

	public void setEstudiantes(List<Estudiante> estudiantes) {
		this.estudiantes = estudiantes;
	}

	public void addEstudiante(Estudiante estudiante) {
		this.estudiantes.add(estudiante);
		estudiante.addMateria(this);
	}
}