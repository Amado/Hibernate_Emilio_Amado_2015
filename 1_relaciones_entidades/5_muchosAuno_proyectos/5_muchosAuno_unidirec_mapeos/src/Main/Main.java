package Main;

import dao.CadenaTelevisivaDAO;
import dao.TelevidenteDAO;
import modelo.CadenaTelevisiva;
import modelo.Televidente;


public class Main {

	public static void main(String[] args) {

	    /* Creamos los tres objetos CadenaTelevisiva */

	    CadenaTelevisiva cadena1 = new CadenaTelevisiva();
	    cadena1.setNombre("Cadena 1");

	    CadenaTelevisiva cadena2 = new CadenaTelevisiva();
	    cadena2.setNombre("Cadena 2");

	    CadenaTelevisiva cadena3 = new CadenaTelevisiva();
	    cadena3.setNombre("Cadena 3");


	    /* Guardamos estos tres objetos CadenaTelevisiva en la base de datos */
	    
	    CadenaTelevisivaDAO.insertCadenaTelevisiva(cadena1);
	    CadenaTelevisivaDAO.insertCadenaTelevisiva(cadena2);
	    CadenaTelevisivaDAO.insertCadenaTelevisiva(cadena3);
	    
	    /* Creamos dos objetos Televidente */
	    
	    Televidente televidente1 = new Televidente();
	    televidente1.setNombre("Televidente 1");
	    televidente1.setCadenaFavorita(cadena1);

	    Televidente televidente2 = new Televidente();
	    televidente2.setNombre("Televidente 2");
	    televidente2.setCadenaFavorita(cadena3);
	    
	    TelevidenteDAO.insertTelevidente(televidente1);
	    TelevidenteDAO.insertTelevidente(televidente2);

	}

}
