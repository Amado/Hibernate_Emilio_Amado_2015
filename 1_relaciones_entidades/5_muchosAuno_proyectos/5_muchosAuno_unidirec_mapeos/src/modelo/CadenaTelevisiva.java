package modelo;

public class CadenaTelevisiva {
	private long id;
	private String nombre;

	public CadenaTelevisiva() {
	}

	public long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}