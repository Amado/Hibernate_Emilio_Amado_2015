package modelo;

public class Televidente {
	private long id;
	private String nombre;
	private CadenaTelevisiva cadenaFavorita;

	public Televidente() {
	}

	public CadenaTelevisiva getCadenaFavorita() {
		return cadenaFavorita;
	}

	public void setCadenaFavorita(CadenaTelevisiva cadenaFavorita) {
		this.cadenaFavorita = cadenaFavorita;
	}

	public long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
