package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="televidentes")
public class Televidente implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private long id;
	
	@Column(name="NOMBRE")
	private String nombre;

	@ManyToOne
	private CadenaTelevisiva cadenaFavorita;

	public Televidente() {
	}

	public CadenaTelevisiva getCadenaFavorita() {
		return cadenaFavorita;
	}

	public void setCadenaFavorita(CadenaTelevisiva cadenaFavorita) {
		this.cadenaFavorita = cadenaFavorita;
	}

	public long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}