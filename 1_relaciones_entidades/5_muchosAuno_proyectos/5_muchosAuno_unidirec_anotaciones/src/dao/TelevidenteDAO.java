package dao;

import modelo.Televidente;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class TelevidenteDAO {
	
	private static Session session;
	private static Transaction tx;
	
	public static long insertTelevidente(Televidente t) throws HibernateException{
		long id;
		
		try{
			
			iniciaOperacion();
			id = (Long) session.save(t);
			tx.commit();
			
		}catch(HibernateException he){
			manejaExcepcion(he);
			throw he; 
		} finally { 
			session.close(); 
		}
		
		return id;
	}
	
	public static void deleteTelevidente(Televidente t) throws HibernateException{
		
		try{
			
			iniciaOperacion();
			session.delete(t);
			tx.commit();
			
		}catch(HibernateException he){
			manejaExcepcion(he);
			throw he; 
		} finally { 
			session.close(); 
		}
	}
	
	private static void iniciaOperacion() throws HibernateException { 
		session = HibernateUtil.getSessionFactory().openSession(); 
		tx = session.beginTransaction(); 
	}  

	private static void manejaExcepcion(HibernateException he) throws HibernateException { 
		tx.rollback(); 
		throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he); 
	}
	
}
