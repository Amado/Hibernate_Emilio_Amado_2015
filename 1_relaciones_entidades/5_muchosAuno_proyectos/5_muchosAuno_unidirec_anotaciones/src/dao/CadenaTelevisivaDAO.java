package dao;

import modelo.CadenaTelevisiva;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class CadenaTelevisivaDAO {

	private static Session session;

	public static void insertCadenaTelevisiva(CadenaTelevisiva c) {

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.persist(c);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			System.out.println("Ocurrio un error en la capa de acceso a datos"
					+ e);
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static void deleteCadenaTelevisiva(CadenaTelevisiva c) {

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(c);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			System.out.print("Ocurrio un error en la capa de acceso a datos"
					+ e);
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}
