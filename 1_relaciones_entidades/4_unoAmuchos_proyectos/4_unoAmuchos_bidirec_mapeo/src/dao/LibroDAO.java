package dao;

import modelo.Libro;
import modelo.Persona;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class LibroDAO {

	private static Session session;
	private static Transaction tx;
	
	public static long insertLibro(Libro l) throws HibernateException{
		long id;
		
		try{
			
			iniciaOperacion();
			id = (Long) session.save(l);
			tx.commit();
			
		}catch(HibernateException he){
			manejaExcepcion(he);
			throw he; 
		} finally { 
			session.close(); 
		}
		
		return id;
	}
	
	public static void deletelibro(Libro l) throws HibernateException{
		
		try{
			
			iniciaOperacion();
			session.delete(l);
			tx.commit();
			
		}catch(HibernateException he){
			manejaExcepcion(he);
			throw he; 
		} finally { 
			session.close(); 
		}
	}
	
	private static void iniciaOperacion() throws HibernateException { 
		session = HibernateUtil.getSessionFactory().openSession(); 
		tx = session.beginTransaction(); 
	}  

	private static void manejaExcepcion(HibernateException he) throws HibernateException { 
		tx.rollback(); 
		throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he); 
	}

}
