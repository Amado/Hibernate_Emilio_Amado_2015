package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Persona {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String nombre;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private List<Libro> libros = new ArrayList<Libro>();
	
	//OTRAS PROPIEDADES
	//=================
	
	/*
	 * En las anotaciones "OneToMany" se puede indicar la propiedad "fetch" (recuperaci�n)
	 * hay dos tipos : lazy, eager
	 * 
	 * lazy : se recuperar�n los datos de los libros solo en el momento en que expl�citamente 
	 * 		  queramos recuperar los libros
	 * 
	 * eager : los libros ser�n recuperados de forma activa, en el momento en 	que se recuperen
	 * 		   las personas, que son los due�os de los libros.
	 */

	public Persona() {
	}

	public long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List getLibros() {
		return libros;
	}

	public void setLibros(List libros) {
		this.libros = libros;
	}

	public void addLibro(Libro libro) {
		this.libros.add(libro);
	}
}