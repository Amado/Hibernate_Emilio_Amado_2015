package dao;

import modelo.Persona;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class PersonaDAO {

//	private static Session session;
//
//	public static void insertPersona(Persona p) {
//
//		try {
//			session = HibernateUtil.getSessionFactory().openSession();
//			session.beginTransaction();
//			session.persist(p);
//			session.getTransaction().commit();
//		} catch (HibernateException e) {
//			System.out.println("Ocurrio un error en la capa de acceso a datos"
//					+ e);
//			e.printStackTrace();
//		} finally {
//			session.close();
//		}
//	}
//
//	public static void deletePersona(Persona p) {
//
//		try {
//			session = HibernateUtil.getSessionFactory().openSession();
//			session.beginTransaction();
//			session.delete(p);
//			session.getTransaction().commit();
//		} catch (HibernateException e) {
//			System.out.print("Ocurrio un error en la capa de acceso a datos"
//					+ e);
//			e.printStackTrace();
//		} finally {
//			session.close();
//		}
//	}
	
	private static Session session;
	private static Transaction tx;
	
	public static long insertPersona(Persona p) throws HibernateException{
		long id;
		
		try{
			
			iniciaOperacion();
			id = (Long) session.save(p);
			tx.commit();
			
		}catch(HibernateException he){
			manejaExcepcion(he);
			throw he; 
		} finally { 
			session.close(); 
		}
		
		return id;
	}
	
	public static void deletePersona(Persona p) throws HibernateException{
		
		try{
			
			iniciaOperacion();
			session.delete(p);
			tx.commit();
			
		}catch(HibernateException he){
			manejaExcepcion(he);
			throw he; 
		} finally { 
			session.close(); 
		}
	}
	
	private static void iniciaOperacion() throws HibernateException { 
		session = HibernateUtil.getSessionFactory().openSession(); 
		tx = session.beginTransaction(); 
	}  

	private static void manejaExcepcion(HibernateException he) throws HibernateException { 
		tx.rollback(); 
		throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he); 
	}
	
}
