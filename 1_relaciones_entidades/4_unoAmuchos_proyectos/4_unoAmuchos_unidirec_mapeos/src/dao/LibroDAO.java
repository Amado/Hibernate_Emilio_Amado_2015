package dao;

import modelo.Libro;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class LibroDAO {

	private static Session session;

	public static void insertLibro(Libro l) {

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.persist(l);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			System.out.println("Ocurrio un error en la capa de acceso a datos"
					+ e);
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public static void deleteLibro(Libro l) {

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(l);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			System.out.print("Ocurrio un error en la capa de acceso a datos"
					+ e);
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}
