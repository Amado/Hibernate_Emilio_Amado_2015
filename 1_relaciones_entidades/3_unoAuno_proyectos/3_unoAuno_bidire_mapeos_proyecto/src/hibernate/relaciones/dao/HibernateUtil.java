/*
 * HibernateUtil.java
 *
 * Creada el 26-ago-2010, 14:36:18
 *
 * Clase Java desarrollada por Alex para el blog http://javatutoriales.blogspot.com/ el día 26-ago-2010
 *
 * Para informacion sobre el uso de esta clase, asi como bugs, actualizaciones, o mejoras enviar un mail a
 * programadorjavablog@gmail.com
 *
 */
package hibernate.relaciones.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	private static final SessionFactory sessionFactory;

	//en un bloque de inicializaci�n est�tico, inicializamos el objeto
	//Sessionfactory en el momento en que la clase se carga sobre el JVM
	static {
		try {
			sessionFactory = new Configuration().
					configure().//hibernate busca el fichero de config "hibernate.cfg.xml"
					buildSessionFactory();//hibernate carga la config del fichero
		} catch (HibernateException he) {
			System.err.println("Ocurri� un error en la inicializaci�n de la SessionFactory: "
							+ he);
			throw new ExceptionInInitializerError(he);
		}
	}

	//retorna la sessionfactory creada
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
