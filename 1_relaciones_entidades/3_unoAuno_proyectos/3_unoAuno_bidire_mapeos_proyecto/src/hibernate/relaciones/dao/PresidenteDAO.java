package hibernate.relaciones.dao;



import hibernate.relaciones.modelo.Presidente;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class PresidenteDAO {
	
	private static Session session;

	public static void insertPersona(Presidente p) {

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.persist(p);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			System.out.println("Ocurrio un error en la capa de acceso a datos" + e);
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public static void deletePersona(Presidente p) {

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(p);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			System.out.print("Ocurrio un error en la capa de acceso a datos" + e);
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
