package hibernate.relaciones.dao;

import hibernate.relaciones.modelo.Direccion;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class DireccionDAO {

	private static Session session;

	public static void insertDireccion(Direccion d) {

		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			session.persist(d);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			System.out.println("Ocurrio un error en la capa de acceso a datos" + e);
			e.printStackTrace();
		} finally {
//			session.close();
			HibernateUtil.closeSession();
		}
	}

}
